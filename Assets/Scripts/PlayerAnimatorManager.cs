﻿using System.Collections;
using UnityEngine;

namespace Com.MyCompany.MyGame
{
    public class PlayerAnimatorManager : MonoBehaviour
    {
        #region Private Fields
        [SerializeField] private float directionDampTime = 0.25f;

        #endregion


        #region MonoBehaviour Callbacks

        private Animator animator;

        void Start()
        {
            animator = GetComponent<Animator>();
            if (!animator)
            {
                Debug.LogError("PlayerAnimatorManager is Missin Animator Component", this);
            }
        }

        void Update()
        {
            if (!animator) { return; }

            float h = Input.GetAxis("Horizontal");
            float v = Input.GetAxis("Vertical");
            if (v < 0) v = 0;
            animator.SetFloat("Speed",h * h + v * v);

            animator.SetFloat("Direction", h, directionDampTime, Time.deltaTime);

            // deal with Jumping
            AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);
            // only allow jumping if we are running.
            if (stateInfo.IsName("Base Layer.Run"))
            {
                // When using trigger parameter
                if (Input.GetButtonDown("Fire2"))
                {
                    animator.SetTrigger("Jump");
                }
            }
        }

        #endregion

    }

}

